//get all user details
const { json } = require("body-parser");
const userModel = require("../models/usermodel");
const userView = async function (req, res) {
    try {
        await userModel.findAll().then(data => {
            res.status(200).json(data)
        })



    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
//get one user detail
const userViewByID = async function (req, res) {
    try {
        const id = req.params.id;
        await userModel.findOne({ where: { id: id } }).then(data => {
            if (data) { res.status(200).json(data) }
            else { throw ("requested user not present in the table") }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
//delete a particular user details
const userDeletionByID = async function (req, res) {
    try {
        const id = req.params.id;
        await userModel.destroy({ where: { id: id } }).then(data => {
            if (data == 1) {
                res.status(200).send("user with id  " + id + " deletd sucessfully");

            }
            else { throw ("requested user not present in the table") }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
//update a user
const userUpdationByID = async function (req, res) {
    try {
        const id = req.params.id;
        const newitem = {
            email: req.body.email,
            state: req.body.state
        }
        await userModel.update(newitem, { where: { id: id } }).then(data => {
            if (data == 1) {
                res.status(200).send("user with id " + id + "updated sucessfully");

            }
            else { throw ("error:updating user with id :" + id) }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
module.exports = { userView, userViewByID, userDeletionByID, userUpdationByID }