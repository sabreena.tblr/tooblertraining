

var express = require('express');
var router = express.Router();
var verifyToken=require('../helpers/userAwtSignature')

var signup = require('../controllers/usercreation')
var { userView, userViewByID, userDeletionByID, userUpdationByID } = require('../controllers/userview')
var usersignin = require('../controllers/usersignin')

router.post('/', signup)
router.get('/view',verifyToken, userView)
router.get('/viewID/:id', verifyToken,userViewByID)
router.delete('/deleteID/:id',verifyToken, userDeletionByID)
router.put('/updateByID/:id',verifyToken, userUpdationByID)
router.post('/signin',usersignin)

module.exports = router