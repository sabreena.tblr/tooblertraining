const jwt = require('jsonwebtoken');

verifyToken = (req, res, next) => {
    try {
        let authHeader = req.headers.authorization
        if (authHeader == undefined) {
            throw ({
                status: 401,
                message: "unauthorized user"
            })
        }

        let token = authHeader.split(" ")[1]
        jwt.verify(token, "secretkey", function (err, decoded) {
            if (err) {
                throw ({
                    status: 403,
                    message: "Authentication failed"
                })
            }

            next()

        })
    }

    catch (e) {
        res.status(e.status).json({
            status: "error",
            message: e.message
        })
    }
}
module.exports = verifyToken