const crypto = require("crypto")

const hash = (password, sec) => {
    return crypto.createHmac('sha256', sec)
        .update(password)
        .digest('hex');
}
module.exports = hash