const sequelize = require('../db/connection').sequelize
const Sequelize = require('sequelize')

module.exports = sequelize.define(
    'user', {
    id: {
        type: Sequelize.INTEGER(10),
        primaryKey: true,
        autoIncrement: true
    },
    fname: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    lname: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    phno: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    gender: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    state: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    password: {
        type: Sequelize.STRING(500),
        allowNull: false
    },
    subscribe: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    secret: {
        type: Sequelize.STRING(20),
        allowNull: false
    }

},
    {
        freezeTableName: true, timestamps: false
    }
);
