const jwt=require("jsonwebtoken")
let jwtSecretKey = "qsedftgh134vbg5t6hgvf5ty6ybvtgvbg"
let jwtTokenGeneration=(user)=>{
    
    
  //console.log("sffg")
    return jwt.sign(user, jwtSecretKey);
  
}
let jwtTokenVerify=(req,res,next)=>{
    try{
    const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) throw({
      status:401,
      message:"unauthorized user"
  })

  jwt.verify(token,jwtSecretKey, (err,token) => {
    // console.log(err)

    if (err)  throw({
        status:403,
        message:"authentication failed"
    })

    // req.user = user

    next()
  })
    }
    catch(e)
    {
        res.status(e.status).json({
            status:"error",
            message:e.message
        })
    }
}
module.exports={jwtTokenGeneration,jwtTokenVerify}