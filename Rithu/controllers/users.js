const express = require('express');
const app = express();
var conn = require('../config/mysql');
const path = require('path');
var encrypt = require('../helper/encrypt');
const userModel = require('../models/userTable');
const {jwtTokenGeneration}=require('../helper/jwt')




module.exports.getpage = function (req, res) {
  // res.sendFile(path.join(__dirname,'/form.html'));
  //res. sendFile(path. join(__dirname, ‘../public’, ‘index1. html’));
  res.sendFile('form.html', { root: path.join(__dirname, '../public') });

}
module.exports.getLoginPage = function (req, res) {
  // res.sendFile(path.join(__dirname,'/form.html'));
  //res. sendFile(path. join(__dirname, ‘../public’, ‘index1. html’));
  res.sendFile('login.html', { root: path.join(__dirname, '../public') });

}
module.exports.createLogin=async function(req,res){
  var Email=req.body.email;
  var password=req.body.password;
  
  let user=await userModel.findOne({where:{mail:Email}})
  
  if(user){
    let hashp=encrypt(password,user.secret);
    if(hashp==user.password){
   let token=jwtTokenGeneration(user.mail)
      res.status(200).json({
      
        message: "login Sucess",
        tokenData:token,
  
      })
  } 
  else{
    res.status(400).json({ "error": "password incorrect" })
  }
}
else{
  res.status(400).json({ "error": "enter valid email" })
}

}
module.exports.createUser = function (req, res) {
  var f_name = req.body.fname;
  var l_name = req.body.lname;
  var email = req.body.email;
  var number = req.body.number;
  var gender = req.body.gender;
  var state = req.body.state;
  var password = req.body.password;
  let secret = Math.random().toString(36).slice(-8);
  let hashp = encrypt(password, secret);




  const regex = /^[a-zA-Z ]{2,30}$/;
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;



  if (!f_name || f_name.trim() == "" || regex.test(f_name) == false || !l_name || l_name.trim() == "" || regex.test(l_name) == false) {
    return res.status(400).json({ "error": "enter valid name" });
  }
  else if (email.length == 0 || mailformat.test(email) == false) {
    return res.status(400).json({ "error": "enter valid email" });
  }

  else if (number.length == 0 || number.length != 10) {
    return res.status(400).json({ "error": "enter valid number" });
  }
  else if (gender.length == 0 || state.length == 0 || password.length == 0) {
    return res.status(400).json({ "error": "cant leave field empty" });
  }
  else {
    // var sql = "INSERT INTO log2 (firstname, lastname, mail,phonenum, gender,state,password) VALUES ('"+f_name+"', '"+l_name+"', '"+email+"', '"+number+"','"+gender+"', '"+state+"',' "+hashp+"')";
    // conn.query(sql, function(err, result) {
    //   if (err) throw err;
    //   console.log('record inserted');
    //   res.send("Data inserted"+JSON.stringify(req.body));

    // });
    userModel.create({
      firstname: f_name,
      lastname: l_name,
      mail: email,
      phonenum: number,
      gender: gender,
      state: state,
      password: hashp,
      secret: secret
    }).then(data => {
      res.status(200).json({
        status: "Sucess",
        message: "data uploaded",
        data: data
      })
    })
  }
}
module.exports.getAllUsers = (req, res) => {
  userModel.findAll().then(data => {
    res.status(200).json({
      status: "Sucess",
      message: "data received",
      data: data
    })
  })
}
module.exports.getUserById = (req, res) => {
  userModel.findOne({ where: { id: req.params.id } }).then(data => {
    res.status(200).json({
      status: "Sucess",
      message: "data received",
      data: data
    })
  })
}
module.exports.updateUser = (req, res) => {
  console.log('hello')
  userModel.update({ firstname: req.body.fname, lastname: req.body.lname }, { where: { id: req.params.id } }).then(data => {
    if (data == 1) {
      res.status(200).json({
        status: "Sucess",
        message: "row updated"
      })
    }
    else {
      res.status(400).json({
        status: "Error",
        message: "row already updated or not found"
      })
    }
  })
}
module.exports.deleteUser = (req, res) => {
  console.log('hello')
  userModel.destroy({ where: { id: req.params.id } }).then(data => {
    if (data == 1) {
      res.status(200).json({
        status: "Sucess",
        message: "row deleted"
      })
    }
    else {
      res.status(400).json({
        status: "Error",
        message: "row already deleted or not found"
      })
    }
  })
}


