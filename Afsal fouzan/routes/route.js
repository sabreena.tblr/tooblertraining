// const user = require("../controllers/user")


const user2 = require("../controllers/user2")
const express = require("express");
const Create = require("../controllers/user2");
const router = express.Router();

const {jwtTokenVerify} = require("../helper/jwt")

router.get("/user/page", user2.display);

router.get("/user", user2.getTable);
router.post("/createuser",user2.Create);
router.put("/updateuser/:id",jwtTokenVerify, user2.updateuser);
router.delete("/deleteuser/:id", user2.deleteuser);
router.get("/userdetails/:id", user2.userdetails);
router.post("/loginuser",user2.logindetails);
router.get("/loginpage", user2.userlogin);


module.exports = router;
 