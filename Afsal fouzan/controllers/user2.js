var express = require("express");
var app = express();
var path = require("path");
const {jwtTokenGeneration} = require ("../helper/jwt");
const crypto = require("crypto");
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const use = require("../model/usermode");

module.exports.display = (req, res, next) => {
  res.sendFile("sign.html", { root: path.join(__dirname, "../public") });
};

module.exports.userlogin = (req, res, next) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../public") });
};

module.exports.getTable = async (req, res) => {
  var q = await use.userModel.findAll();
  res.send(q);
};

module.exports.Create = (req, res) => {
  const body = req.body;
  console.log(body);
  const fname = body.fname;
  const lname = body.lname;
  const email = body.email;
  const number = body.number;
  const gender = body.gender;
  const state = body.state;
  const password = body.password;
  const secret = Math.random().toString(36).slice(-8);
  const hashedpass = crypto
    .createHmac("sha256", secret)
    .update(password)
    .digest("hex");

  var name_pattern = /^[a-zA-Z]+$/;
  var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (
    !fname ||
    fname.trim() == "" ||
    name_pattern.test(fname) == false ||
    !lname ||
    lname.trim() == "" ||
    name_pattern.test(lname) == false ||
    !email ||
    email.trim() == "" ||
    email_pattern.test(email) == false ||
    ((number || number.trim() != "") &&
      (number.length != 10 || isNaN(number))) ||
    !gender ||
    gender.trim() == "" ||
    ((password || password.trim() != "") && password.length < 5)
  ) {
    res.send("check your input fields");
  } else {
    use.userModel
      .create({
        first_name: fname,
        last_name: lname,
        email_id: email,
        phone_no: number,
        gender: gender,
        state: state,
        password: hashedpass,
        secret: secret,
      })
      .then((data) => {
        res.send(data);
      });
  }
};

module.exports.updateuser = (req, res) => {
    if(!req.body.fname){
        res.status(400).json({error:"please enter the data to be updated"})
    }
    else{
        use.userModel
        .update(
          {
            first_name: req.body.fname,
          },
          {
            where: { id: req.params.id },
          }
        )
        .then((data) => {
          if (data == 1) {
            res.status(200).json({
              status: "success",
              message: "updated",
            });
          }
           else {
            res.status(400).json({
              status: "error",
              message: "does not exist",
            });
          }
        });
    }

};

module.exports.deleteuser = (req, res) => {
  use.userModel
    .destroy({
      where: { id: req.params.id },
    })
    .then((data) => {
      if (data == 1) {
        res.status(200).json({
          status: "success",
          message: "deleted",
        });
      } else {
        res.status(400).json({
          status: "error",
          message: "does not exist",
        });
      }
    });
};

module.exports.userdetails = async (req, res) => {
  var q = await use.userModel.findOne({
    where: { id: req.params.id },
  });
  res.send(q);
};

module.exports.logindetails = async (req, res) => {
  try {
    if (!req.body.email || req.body.email.trim() == "") {
      return res.status(400).json({ error: "email  is required" });
    } else if (!req.body.password || req.body.password.trim() == "") {
      return res.status(400).json({ error: "password  is required" });
    }
    const logemail = req.body.email;
    const logpassword = req.body.password;

    const user = await use.userModel.findOne({
      attributes: ["email_id", "password", "secret"],
      where: { email_id: logemail },
    });

    if (user) {
      const dbpassword = user.password;
      const dbsecret = user.secret;
      const hashedpass = crypto
        .createHmac("sha256", dbsecret)
        .update(logpassword)
        .digest("hex");
      // if (hashedpassword == dbpassword) {
      // //   res.send("successfully loggedin")
      if (hashedpass == dbpassword) {
        let token = jwtTokenGeneration(user)
        res.status(200).json({
            message: "loggedin",
            tokenData: token
          });
      } else {
        res.json({error : "password incorrect"});
      }
    } else res.json({error :"user doesn't exist!!"});
    
  } catch (err) {
     // throw err
    console.error(err)
  }
}; 
