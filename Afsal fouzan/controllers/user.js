var express = require("express");
var app = express();
var con = require("../config/db")
var path = require("path");
const crypto = require("crypto");
var { check, validationResult } = require("express-validator");

module.exports.display = (req, res, next) => {
    // res.sendFile(path.join(__dirname, "sign.html"));
    // res.sendFile(path.resolve('/home/toobler/Desktop/tooblertraining/Afsal fouzan/sign.html'))
    res.sendFile('sign.html', { root: path.join(__dirname, '../public') });
}
  
  module.exports.create = (req, res) => {
      const body = req.body;
      // check("fname","invalid")
      //     .exists()
      //     .isLength({min : 3})
      console.log(body);
      // const err = validationResult(req);
      // if(!err.isEmpty()){
        const fname = body.fname;
        const lname = body.lname;
        const email = body.email;
        const number = body.number;
        const gender = body.gender;
        const state = body.state;
        const password = body.password;
        const secret = "afsal";
        const hashedpass = crypto.createHmac('sha256',secret).update(password).digest('hex')
        
        var name_pattern = /^[a-zA-Z]+$/
        var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        
        if(fname.length==0 || name_pattern.test(fname)==false 
        || lname.length==0 || name_pattern.test(lname)==false
        ||email.length==0 || email_pattern.test(email)==false
        || number.length == "" || number.length != 10 || isNaN(number)
        || gender == "" || password.length == "" || password.length<5){
            res.send("check your input fields");
        }
        else {
          const db = "insert into signup (first_name, last_name,email_id, phone_no, gender, state, password) values ('"+fname + "','" +lname +"','" +email +"','" +number +"','" +gender +"', '" +state + "','" +hashedpass +"')";
          con.query(db, (err, result) => {
            if (err) throw err;
            console.log(result);
          });
        
          res.send("one row inserted" + JSON.stringify(body));
          res.end();
        }
    }