// var con=require('../route/connection')
const crypto = require('crypto');
const { userModel } = require("../models/userModel");


const userSignup = async function (req, res) {
    try {
       
        let array = ["f_name", "l_name", "email", "phone", "gender", "state", "pass", "sub"]
        array.forEach((field) => {
            if ((!req.body[field]) || (req.body[field].trim() == "")) {
                throw (`error:${field} is required`)
            }
        })
        var regex = /^[A-Za-z]+$/;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var phregx = /^[0-9-+]+$/;
        var passreg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (regex.test(req.body.f_name) == false) {
            throw ("error:name must contain only alphabets")
        }
        else if (emailReg.test(req.body.email) == false) {
            throw ("error:Email pattern not matching")
        }
        else if (regex.test(req.body.l_name) == false) {
            throw ("error:Last name must contain only alphabets ")
        }

        else if (phregx.test(req.body.phone) == false) {
            throw ("error:phone number must match pattern")
        }
        else if (passreg.test(req.body.pass) == false) {
            throw ("error:password  must match Minimum eight characters, at least one letter and one number")
        }
        else if (req.body.pass != req.body.cpass) {
            throw ("Password didn't match")
        }
        else {

            const secret = Math.random().toString(36).slice(-8);
            const hash = crypto.createHmac('sha256', secret)
                .update(req.body.pass)
                .digest('hex');
            //  console.log(hash);
            let { id, f_name, l_name, email, phone, gender, state, sub } = req.body
            await userModel.create({
                f_name: f_name,
                l_name: l_name,
                email: email,
                phone: phone,
                gender: gender,
                state: state,
                pass: hash,
                sub: sub,
                secret: secret
            }).then(function (userModel) {
                console.log("passed")
                if (userModel) {
                    res.status(200).json(userModel);
                } else {
                    throw('Error in insert new record');
                }
            });
        }

    }
    catch (e) {
        res.status(400).json({
            status:"error",
            message:e
        })
    }
}
module.exports = userSignup