const Sequelize = require("sequelize") ;
require("dotenv").config();
let sequelize = new Sequelize(
  'college',
  'root',
  'password',
  {
    dialect: "mysql",
    operatorsAliases: 0,
    logging: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    replication: {
      read: [
        {
          host: 'localhost',
          username: 'root',
          password: 'password',
        },
      ],
      write: {
        host: 'localhost',
        username: 'root',
        password: 'password',
      },
    },
  }
);
module.exports.sequelize=sequelize

