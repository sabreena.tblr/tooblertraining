const sequelize = require('../db/connection').sequelize
const Sequelize  = require("sequelize");
let userModel = sequelize.define(
    "user_dtls",
    {
        id: {
            type: Sequelize.INTEGER(30),
            primaryKey: true,
            autoIncrement: true

        },
        f_name: {
            type: Sequelize.STRING(45),
            allowNull: false

        },

        l_name: {
            type: Sequelize.STRING(45),
            allowNull: false
        },
        email:{
            type: Sequelize.STRING(40),
            allowNull: false
        },
        phone:{
            type: Sequelize.STRING(10),
            allowNull: false
        },
        gender:{
            type: Sequelize.STRING(30),
            allowNull: false
        },
        state:{
            type: Sequelize.STRING(40),
            allowNull: false
        },
        pass:{
            type: Sequelize.STRING(100),
            allowNull: false
        },
        sub:{
            type: Sequelize.TINYINT(),
            allowNull: false
        },
        secret:{
            type: Sequelize.STRING(45),
            allowNull: false
        }


    },
    {
        freezeTableName: true, timestamps: false
    }
)
module.exports.userModel=userModel