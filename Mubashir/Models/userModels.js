const sequelize = require('../config/connection').sequelize
const Sequelize  = require("sequelize");
let userModel = sequelize.define(
    "Login_data",
    {
        id: {
            type: Sequelize.INTEGER(),
            primaryKey: true,
            autoIncrement: true

        },
        Username: {
            type: Sequelize.STRING(45),
            allowNull: false

        },

        Password: {
            type: Sequelize.STRING(500),
            allowNull: false
        },
        Email:{

            type: Sequelize.STRING(45),
            allowNull: false
        },
        
        
        Firstname:{

            type: Sequelize.STRING(45),
            allowNull: false
        },
        Lastname:{

            type: Sequelize.STRING(45),
            allowNull: false
        },
         secret:{
             type: Sequelize.STRING(500),
             allowNull: false
         }

    },
    {
        freezeTableName: true, timestamps: false
    }
)
module.exports.userModel=userModel