const signup = require('../controllers/User')
var express = require('express');
var router = express.Router();
const {jwtTokenVerify}=require("../helper/jwtToken")
//  var con = require('../config/Db');
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())
const path = require('path');
 var userLogin = require('../controllers/userLogin')



const { getUsers, getuserByID, userDeletionByID, userUpdationByID } = require('../controllers/view');





// router.get('/signup', (req, res, next) => {
//      res.sendFile(path.join(__dirname, '/index.html'));
// });





 router.post('/userlogin',userLogin)
router.post('/', signup)
router.get('/view',jwtTokenVerify, getUsers)
router.get('/viewID/:id',jwtTokenVerify, getuserByID)
router.delete('/deleteID/:id', jwtTokenVerify,userDeletionByID)
router.put('/updateByID/:id',jwtTokenVerify,userUpdationByID)



module.exports = router