const userModel = require('../Models/userModels').userModel;

const crypto = require('crypto');
const signup = async (req, res) => {
    try {
        let user = {}
        let array = ["Firstname", "Lastname", "Password", "Username", "Email"]
        array.forEach((field) => {
            if ((!req.body[field]) || (req.body[field].trim() == "")) {
                //return res.status(400).json({"error":`${field} is required`})
                throw (`error:${field} is required`)

            }
            else {
                user[field] = req.body[field]
            }
        })



        // var Firstname = req.body.Firstname
        // var Lastname = req.body.Lastname
        // var Password = req.body.Password
        // var uName = req.body.Username
        // var Email = req.body.Email
        // var postData = JSON.parse(JSON.stringify(req.body))

        // encription
        const secret = Math.random().toString(36).slice(-8);
        const hash = crypto.createHmac('sha256', secret)
            .update(Password)
            .digest('hex');
        // console.log(hash);
        user["Password"] = hash
        user["secret"]=secret
        console.log("hhbj", user);

        // validation
        var regex = /^[A-Za-z]+$/;
        var EmailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        // var phregx = /^[0-9-+]+$/;
        if (
            regex.test(Firstname) == false
            || Lastname.length == 0
            || regex.test(Lastname) == false
            // || Email.length == 0
            || EmailReg.test(Email) == false


            || Password.length == 0
            || Password.length < 6)
            res.send("Name Fields can not be empty, 1.Name field can only be alphabet, 2.Email fields cannot be empty, 3.Password fields cannot be empty")


        else {


            // var sql = "INSERT INTO Login_data (Username, Password,Email,Firstname, Lastname ) VALUES ('" + uName + "', '" + hash + "', '" + Email + "',  '" + Firstname + "', '" + Lastname + "')";
            // con.query(sql, (err, result) => {
            //     console.log(postData);
            //     res.send(postData)
            //     if (err) {
            //         throw err;

            //     }
            // })


            let { id, Username, Password, Email, Firstname, Lastname } = req.body
            return await userModel.create(user).then(function (userModel) {
                if (userModel) {
                    res.send(userModel);
                } else {
                    res.status(400).send('Error in insert new record');
                }
            });
        }
    } catch (err) {
        res.send(err)
    }
}
module.exports = signup
