var express  = require("express");
var app = express();

const path = require("path");
var bodyparser = require("body-parser");
app.use(bodyparser.urlencoded({extended:true}));
var router=require('./router/user')
app.get('/signupform',(req,res,next)=>{
    res.sendFile(path.join(__dirname,'/signup.html'));
});

app.use('/router',router)
app.listen(8003);