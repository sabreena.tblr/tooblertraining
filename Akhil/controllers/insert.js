var hash= require('../helper/encryption')
var con= require('../database/connection');
var insertion= (req,res)=>{
    const body =req.body;
    console.log(body);
    const fname= body.fname;
    const lname=body.lname;
    const email=body.email;
    const phone = body.contact;
    const check =body.check;
    const gender =body.gender;
    const confirm=body.confirmPassword;
    const state =body.state;
    const subscribe =Boolean(body.subscribe);
    const password= body.password;
    const pass = hash(password);

    var name_pattern = /^[a-zA-Z]+$/
    var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    
    if(fname.length==0 || name_pattern.test(fname)==false 
    || lname.length==0 || name_pattern.test(lname)==false
    ||email.length==0 || email_pattern.test(email) ==false
    || phone.length == "" || phone.length != 10 || isNaN(phone)
    || gender == ""
    || check==""
    || password.length == "" || password.length<5
    || confirm.length == "" || confirm != password){
        res.send("check your input fields");
    }
    else {
        let sql=`INSERT INTO login (FirstName,LastName,Email,Contact,Gender,State,Subscribe,password) VALUES ("${fname}","${lname}","${email}","${phone}","${gender}","${state}","${subscribe}","${pass}")`
        con.query(sql,(err,result)=> {
            if(err)throw err;
            console.log("Values Added",result);
        });
    }
}
module.exports=insertion;